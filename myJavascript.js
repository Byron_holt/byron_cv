
function calculateAge() {

  var input = document.getElementById("calc").value;

  var birth = input.split("-")

  var current = [new Date().getFullYear(), new Date().getMonth(), new Date().getDate()];


  var y = current[0] - birth[0];
  var m = (current[1] - birth[1]) + 1;
  var d = current[2] - birth[2];

  // calculate month and days     

  if (m < 0) {
    m = 12 + m;
  }

  if (d < 0) {
    var x;
    m--;
    if ((m > 6 && m % 2 == 1) || (m < 6 && m % 2 == 0)) {
      x = 30;
    } else {
      x = 31;
    }
    d = x + d;
  }

  document.getElementById("calc").value = "You are " + y + " years, " + m + " months and " + d + " days old!";
}







function changeCookie()
{
   
    if(getCookie("state")=="common.css")
    {
      setCookie("state", "common2.css", 30);
    }else{
      setCookie("state", "common.css", 30);
    }
    checkCookie();
}


function setCookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname+"="+cvalue+"; "+expires;
}

function getCookie(cookie) {
    var name = cookie + "=";
    var cookieArray = document.cookie.split(';');
    for(var i = 0; i < cookieArray.length ; i++) {
        var c = cookieArray [i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var a = document.getElementById("style1");
    var state=getCookie("state");
    if (state != "") {
        a.href = state;
    } else {
           setCookie("state", "common.css", 30);
       }
    
}